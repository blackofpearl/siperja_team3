<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('kecamatan/{id}', function ($id) {
    $course = App\Models\tbl_kecamatan::where('id_kota',$id)->get();
    return response()->json($course);
});

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('index');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('index');

//Users
Route::get('/users', [App\Http\Controllers\usersmanagementController::class, 'index'])->name('master.usersmanagement.users.index');
Route::get('/register', [App\Http\Controllers\usersmanagementController::class, 'register'])->name('master.usersmanagement.users.register');
Route::post('/create_users', [App\Http\Controllers\usersmanagementController::class, 'create_users'])->name('master.usersmanagement.users.create_users');
Route::get('/destroy_users/{id}', [App\Http\Controllers\usersmanagementController::class, 'destroy_users'])->name('master.usersmanagement.users.destroy_users');
Route::get('/edit_users/{id}', [App\Http\Controllers\usersmanagementController::class, 'edit_users'])->name('master.usersmanagement.users.edit_users');
Route::post('/update_users/{id}', [App\Http\Controllers\usersmanagementController::class, 'update_users'])->name('master.usersmanagement.users.update_users');
Route::get('/roles/add', [App\Http\Controllers\usersmanagementController::class, 'add'])->name('master.usersmanagement.roles.add');
Route::get('/roles/edit/{id}', [App\Http\Controllers\usersmanagementController::class, 'edit'])->name('master.usersmanagement.roles.edit');
Route::post('/roles/create', [App\Http\Controllers\usersmanagementController::class, 'create'])->name('master.usersmanagement.roles.create');
Route::post('/roles/update/{id}', [App\Http\Controllers\usersmanagementController::class, 'update'])->name('master.usersmanagement.roles.update');
Route::get('/roles/destroy/{id}', [App\Http\Controllers\usersmanagementController::class, 'destroy'])->name('master.usersmanagement.roles.destroy');


//penjadwalan
Route::get('/scheduler', [App\Http\Controllers\schedulerController::class, 'index'])->name('master.scheduler.index');

//keuangan
Route::get('/keuangan', [App\Http\Controllers\keuanganController::class, 'index'])->name('master.keuangan.index');

//verifikasi
Route::get('/verifikasi', [App\Http\Controllers\verifikasiController::class, 'index'])->name('master.verifikasi.index');

//pengajar
Route::get('/pengajar', [App\Http\Controllers\pengajarController::class, 'index'])->name('master.scheduler.index');