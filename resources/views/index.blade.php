@extends('layouts.lay')

@section('content')

                <div class="content-header-left col-12 mb-2 mt-1">
                    <div class="breadcrumbs-top">
                        <h5 class="content-header-title float-left pr-1 mb-0">Dashboard</h5>
                        <div class="breadcrumb-wrapper d-none d-sm-block">
                            <ol class="breadcrumb p-0 mb-0 pl-1">
                                <li class="breadcrumb-item"><a href="<?php echo url("/"); ?>"><i class="bx bx-home-alt"></i></a>
                                </li>
                                <li class="breadcrumb-item active">Home
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
                    <div class="row">
                        <!-- Task Card Starts -->
                        <div class="col-lg-7">
                            <div class="row">
                                <div class="col-12">
                                  
                                <div class="row">

                                <div class="col-sm-6 col-12 dashboard-users-success">
                                            <div class="card text-center">
                                                <div class="card-body py-1">
                                                    <div class="badge-circle badge-circle-lg badge-circle-light-success mx-auto mb-50">
                                                    <i class="menu-livicon" data-icon="users"></i>
                                                    </div>
                                                    <div class="text-muted line-ellipsis">Users</div>
                                                    <h3 class="mb-0"><?php 
                                                    $count = \DB::table('users')
                                                    ->select(\DB::raw('COUNT(id) as hit'))
                                                    ->get();
                                                    ?>
                                                    @foreach($count as $cc){{$cc->hit}}@endforeach
                                                  </h3>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6 col-12 dashboard-users-success">
                                            <div class="card text-center">
                                                <div class="card-body py-1">
                                                    <div class="badge-circle badge-circle-lg badge-circle-light-success mx-auto mb-50">
                                                    <i class="menu-livicon" data-icon="diagram"></i>
                                                    </div>
                                                    <div class="text-muted line-ellipsis">Role Users</div>
                                                    <h3 class="mb-0"><?php 
                                                    $count = \DB::table('roles')
                                                    ->select(\DB::raw('COUNT(id) as hit'))
                                                    ->get();
                                                    ?>
                                                    @foreach($count as $cc){{$cc->hit}}@endforeach
                                                  </h3>
                                                </div>
                                            </div>
                                        </div>
                                        
                                </div>


                                </div>
                            </div>
                        </div>
                    </div>
@endsection